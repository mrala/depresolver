# Problem: Package dependencies
#
# There are a lot of different ways to install packages on different operating
# systems. One thing that most of them have in common is a way to handle
# dependencies. When one package depends on another package the dependency
# must be installed first. The following text file lists packages (A,B,C,..etc)
# followed by their corresponding package dependencies.
#
# example: deps.txt
# A:D,B
# B:C,E
# C:D,E
#
#      A
#     /\
#    B  \
#   /\   \
#  /  C   \
# / /   \  \
# E       D
#
# First task is to read in the text file, populating a data structure
# where keys are the package names and the values are an array of dependencies.
# Next is to build the dependency management part of a package manager.
# For example, to install package C you must also install package D and E.
# Write a function get_deps() that given a package name returns a list of
# package dependencies.
#
class DepResolver
  def initialize
    @package_hash = {}
    File.open('deps.txt', 'r') do |f|
      f.each_line do |line|
        package, dependencies = line.match(/^(.*):(.*)$/).captures
        @package_hash[package] = dependencies.split(',')
      end
    end
  end

  def get_deps(item)
    deps = []
    deps = get_nested(item)
    deps.each do |dep|
      # Prevent circular dependencies
      deps |= get_nested(dep)
    end
    deps.uniq!
    deps
  end

  def get_nested(items)
    if items.is_a?(Array)
      items.each do |item|
        get_nested(item)
      end
    elsif items.is_a?(String)
      @package_hash[items] ? @package_hash[items] : []
    end
  end
end

# resolv = DepResolver.new
# resolv.get_deps('A')
# => ["D", "B", "C", "E"]
# resolv.get_deps('B')
# => ["C", "E", "D"]
# resolv.get_deps('C')
# => ["D", "E"]
